import java.util.List;

public class DemoPointsCircle {
    public static void main(String[] args) {
//      Point point = new Point();
        PointList pointList = new PointList();
        Circle circle = new Circle();
        List<PointList> listOfPoints = pointList.inputPoints();
        System.out.println("\n\n\n");
        listOfPoints.forEach(value-> System.out.println("x - " + value.x + " \ny - " + value.y));

        System.out.println("\n\n\n");

        List<PointList>  filteredListOfPoints = pointList.isPointInsideircle(circle.centerPoint,listOfPoints,circle.radius);
        filteredListOfPoints.forEach(value-> System.out.println("x - " + value.x + " \ny - " + value.y));


    }
}
