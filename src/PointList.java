import java.util.*;

import static java.lang.Math.sqrt;

public class PointList extends Point{

    /*public PointList() {
        );
    }*/
    

    public List<PointList> inputPoints(){
        List<PointList> listPoints = new ArrayList<>();
        int input;
        do {
            PointList p = new PointList();
            System.out.println("Input x:");
            Scanner scanner = new Scanner(System.in);
            p.x = scanner.nextInt();
            System.out.println("Input y:");
            p.y = scanner.nextInt();

            listPoints.add(p);
            int counter =0;
            System.out.println(listPoints.get(counter).x);
            System.out.println(listPoints.get(counter).y);

            System.out.println("Желаете добавить точку? 1 - да 2 - нет");
            input = scanner.nextInt();
            if (input!=1&&input!=2){
                while(input!=1&&input!=2){
                    System.out.println("Incorrect input, please input 1 or 2");
                    input = scanner.nextInt();
                }
            }
            counter++;
        }while (input!=2);
        return listPoints;
    }

    public boolean isPointInsideircle(Point center, Point point, int radius){
        boolean isPointInside = Math.abs(sqrt(Math.pow((center.getX()-point.getX()),2)+Math.pow((center.getY()-point.getY()),2)))<=radius;

        return isPointInside;
    }

}
